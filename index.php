<?php 
// we need external file with PHP functions
require_once('functions.php');
// read currency value from $_GET
$currency = $_GET["currency"];
?>
<html>
<head>
    <title>Kursy walut NBP</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</head>
<body>
<div class="container">
    <head>
        <h1>Tabela średnich kursów walut NBP</h1>
        <h4>NBP publikuje dane codziennie od poniedziałku do piątku w godz. między 11:45 a 12:15</h3>
    </head>
    <hr />
    <div class="row">
        <form class="form-inline">
            <div class="form-group">
            	<label for="getCurrency" value="Wybierz walute">Wybierz walutę:</label>
            	<select id="choose" onchange="getState(this.value)">
        	<div class="form-group">
        	   <option value="">wybierz walutę</option>
        	   <option value="usd"<?php if($currency == "usd") { echo ' selected="selected"'; } ?>>USD</option>
        	   <option value="eur"<?php if($currency == "eur") { echo ' selected="selected"'; } ?>>EUR</option>
        	   <option value="gbp"<?php if($currency == "gbp") { echo ' selected="selected"'; } ?>>GBP</option> 
        	</select>
        </form>
        <table id="exchangeRates" class="table table-striped table-condensed">
        <tbody>
            <tr class="first" align="center">
                <td><strong>Dzień</strong></td>
                <td><strong>Kurs [zł]</strong></td>
            </tr>         
            <?php
        
            $i = 0;
            $count = 0;
            do {
                $date = date("Y-m-d", strtotime('-' . $i . 'days'));
                $dayNum = date("N", strtotime('-' . $i . 'days'));

                $exchangeRate = getExchangeRate($date, $currency);
                
                // we need to skip Saturdays and Sundays and time before 12:15pm, 
                // because NBP is publishing Exchange rates only on weekdays and between 11:45am 12:15pm.
                if ( $dayNum > 5 || ( date("Hi") < "1215" && $i == 0) ) {
                    $i++;
                    continue;
                }
                
            ?>
                    <tr align="center">
                        <td><?php echo $date; ?></td>
                        <td><?php echo $exchangeRate; ?></td>
                    </tr>
            <?php
                // build an array for colour MAX and MIN value in table
                // it will be used later in jQuery scirpt
                $ratesArray[$count] = $exchangeRate;
                $i++;
                $count++;
            }
            // do loop till 10 rows has been reached ($count variable)
            // and protect loop if something go wrong ($i variable)
            while ($count < 10 && $i < 20);
            
            ?>
        </tbody>
        </table>
    </div>
    <footer>
        <p>Created by: Sebastian Gruszka</p>
    </footer>
</div>

<script type="text/javascript">

// refresh view after currency choose in <select> list
function getState(currency)
{
    var html = $.ajax({
        type: "GET",
        url: "index.php",
        data: "currency=" +currency,
        async: false
    }).responseText;
    if(html){
        $(".container").html(html);
    }
} 

/**
 * below code is used for colouring RED and GREEN the MAX and MIN values in table
 */
// build array with values and find MAX and MIN value
var jqueryarray = <?php echo json_encode($ratesArray); ?>;
var max = Math.max.apply(Math, jqueryarray);
var min = Math.min.apply(Math, jqueryarray);

// using jQuery to populate the table cells
var table = document.getElementById('exchangeRates');
var tbody = table.getElementsByTagName('tbody')[0];
var cells = tbody.getElementsByTagName('td');

for (var i=0, len=cells.length; i<len; i++){
    if (parseFloat(cells[i].innerHTML) == max){
        cells[i].style.backgroundColor = 'red';
    }
    else if (parseFloat(cells[i].innerHTML) == min){
        cells[i].style.backgroundColor = 'green';
    }
}
/**
 * end code 
 */

</script>

</body>
</html>
