ZADANIE:
 

Temat: Dynamiczne pobieranie kursów walut z NBP.
                
Można korzystać z Google, know-how, gotowych bibliotek, ….
 
Opis zadania:
 
Należy stworzyć tabelkę w html prezentującą średnie kursy jednej, wybranej waluty (pole select):
dolar amerykański (USD), euro (EUR), funt szterling (GBP)
z ostatnich 10 dni.
Wizualizacja:
 
Wybierz walutę: <select>
 
   dzień     |  kurs [zł]
-------------|-------------
2016-11-15   |
-------------|-------------
2016-11-14   |
-------------|-------------
2016-11-11   |
-------------|-------------
. . .        |
-------------|-------------
 
 
Domyślnie ma pojawić się pusta tabela z prośbą o wybranie waluty z „selecta”.
Po wybraniu waluty z listy automatycznie mają zostać pobrane dane ajaxem i wpisane do tabeli.
Dane mają być pobierane dynamicznie z serwera NBP (skrypt php każdorazowo ma się łączyć z NBP)
 
Po załadowaniu danych do tabeli, za pomocą jQuery należy na czerwono zaznaczyć najwyższy kurs waluty, a na zielono kurs najniższy.
 
Instrukcja, jak pobierać kursy walut z NBP:
http://www.nbp.pl/home.aspx?f=/kursy/instrukcja_pobierania_kursow_walut.html
(nie trzeba korzystać z ich API, wystarczy pobieranie xml).