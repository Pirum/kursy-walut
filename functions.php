<?php

/**
 * Validate parameters function
 * 
 * Author: Sebastian Gruszka
 *
 * return bool
 */
function isValid($type, $value) 
{
    $regexp = NULL;
    
    if ( !is_null($value) && isset($type) ) {
        
        switch($type){
            case "date": 
                $regexp = "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/";
                break;
            
            case "currency":
                $regexp = "/^[a-z]{3}$/";
                break;
        }
        
        if (preg_match($regexp, $value)) {
        
            return true;
        }

        return false;
    }
    
    return false;
}

/**
 * Get NBP exchange rates
 * using NBP API
 * documentation: http://api.nbp.pl 
 *
 * Author: Sebastian Gruszka
 *
 * var $date - date for exchange rate
 * var $code - currency code, eg: usd, eur, gbp
 * var $table - by default we need the A table
 * 
 * return float or false
 */
function getExchangeRate($date, $code, $table = 'a' ) {
    
    // validate the variables comes from outside
    if ( isValid("date", $date) && isValid("currency", $code)) {
        // format wyjsciowy
        $format = "json";
        
        // build CURL
        $url = "http://api.nbp.pl/api/exchangerates/rates/" . $table . "/" . $code . "/" . $date . "/?format=" . $format;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_GET, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($curl);
        
        if (!$result) {
        	echo curl_error($curl);
        	
        	return false;
        }
        curl_close($curl);
        
        // create object from json result
        $obj = json_decode($result, true);
        // get exchange rate from object
        $result = $obj['rates'][0]['mid'];
        if ( is_null($result) ) {
            $result = NULL;
        }
        
        return $result;
    }
    
    return false;
}

